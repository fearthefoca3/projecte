﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AudioManager : MonoBehaviour
{
    public PlayerInputs player;
    public AudioSource audioNormal;
    public AudioSource audioBattle;
    [Range(0, 5)] double fight = 0;

    private void Awake()
    {
        audioNormal.Play();
    }

    private void Update()
    {

        if (player.isFighting)
        {
            fight = fight + 1;

            if (fight == 3)
            {

                audioNormal.Stop();
                audioBattle.Play();
            }

            if (player.isFighting == false)
            {
                fight = 0;
                audioBattle.Stop();
                audioNormal.Play();
            }
        }

        /**if (fight == 0)
        {
            
        }**/

       
        

       
        
    }
}
