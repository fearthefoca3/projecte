﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Bomb : MonoBehaviour
{
    public int MaxSpeed;
    public int MinSpeed;
    public int Speed;
    private Rigidbody2D _rigidbody;

    bool _lookingRight;

    public Transform EdgeDetectionPoint;
    public LayerMask WhatIsGround;
    public LayerMask WhatIsBomb;


    public float rayCastLength = 10f;
    int layerMask;
    RaycastHit2D hit;

    public GameObject explosion;

    // Start is called before the first frame update
    void Start()
    {
        Random rnd = new Random();

        Speed = Random.Range(MinSpeed, MaxSpeed);
        _rigidbody = GetComponent<Rigidbody2D>();
    }

    // Update is called once per frame
    void FixedUpdate()
    {
        /*
        layerMask = 1 << WhatIsBomb;

        hit = Physics2D.Raycast(transform.position, -Vector2.right, rayCastLength, layerMask);
        Debug.DrawRay(transform.position, -Vector2.right * rayCastLength, Color.red);

        if (hit)
        {
            Debug.Log("Hit");
        }
        else
        {
            Debug.Log("NO Hit");

        }*/

        if (EdgeDetected())
            Flip();
        Move();
    }

    private void Move()
    {
        float x;
        if (_lookingRight)
            x = Speed * Time.fixedDeltaTime;
        else
            x = -Speed * Time.fixedDeltaTime;
        _rigidbody.velocity = new Vector2(x, _rigidbody.velocity.y);
    }

    private bool EdgeDetected()
    {
        RaycastHit2D hit = Physics2D.Raycast(
            EdgeDetectionPoint.position,
            -Vector2.up,
            1.5f,
            WhatIsGround);
        return hit.collider == null;
    }


    private void Flip()
    {
        transform.Rotate(0, 180, 0);
        if (_lookingRight == false)
        {
            _lookingRight = true;
        }
        else
        {
            _lookingRight = false;
        }
    }

    private void OnTriggerEnter2D(Collider2D other)
    {

        if (other.CompareTag("AtackingWeapon"))
        {
            Explode();
        }
    }

    private void OnCollisionEnter2D(Collision2D other)
    {
        if (other.gameObject.tag == "Bomb")
        {
            Flip();
        }

        if (other.gameObject.tag == "Player")
        {
            Explode();
        }
    }

    public void Explode()
    {
        Instantiate(explosion, transform.position, Quaternion.identity);
        Destroy(gameObject);
        FindObjectOfType<BombSpawner>().actualbombs--;
    }
}
