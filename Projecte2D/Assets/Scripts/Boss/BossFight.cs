﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine.SceneManagement;
using UnityEngine;


public class BossFight : MonoBehaviour
{
    public FightMap fm;
    public int BossHealth;
    public int BossMaxHealth;

    public int NumberCores;

    public GameObject Phase1Container;

    // Start is called before the first frame update
    void Start()
    {
        BossHealth = BossMaxHealth;
    }

    private void OnTriggerEnter2D(Collider2D other)
    {
        fm.SetPlayerAndEnemyToFightMap(other.gameObject,null);
        other.GetComponent<PlayerInputs>().SetFightingStat(true);
        Phase1Container.SetActive(true);
    }

    private void phase1()
    {

    }

    public void CoreDestroyed()
    {
        NumberCores--;

        if (NumberCores <= 0)
        {
            Debug.Log("fin fase 1");
            SceneManager.LoadScene("Credits");
            //SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex -1);
            // BossHealth--;
        }
    }
}
