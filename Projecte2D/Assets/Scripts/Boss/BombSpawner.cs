﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BombSpawner : MonoBehaviour
{
    public GameObject bomb;
    [Range(0,1)]public float minTime;
    [Range(1,5)]public float maxTime;
    public int maxBombs = 5;
    public int actualbombs = 0;

    public float spawnCd = 1f;
    float currentspawnCd = 0f;

    private void FixedUpdate()
    {
        if (actualbombs < maxBombs)
        {
            currentspawnCd -= Time.deltaTime;

            if (currentspawnCd <= 0)
            {
                currentspawnCd = spawnCd;
                spawnBomb();
                actualbombs++;
            }
        }
    }

    private void spawnBomb()
    {
        Instantiate(bomb, this.transform.position, Quaternion.identity);
    }

}
