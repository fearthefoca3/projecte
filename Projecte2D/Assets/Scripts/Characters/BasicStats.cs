﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.InputSystem;

public class BasicStats : MonoBehaviour
{
    [Header("Parent Class Vars")]
    [Header("Vars to move")]
    public float x, y;
    public bool facingRight;
    public float runSpeed;
    public bool canWeMove;
    [Header("Vars to fight")]
    public int health;
    public int maxHealth;
    public int basicAtack; 
    public bool isFighting;
    public bool isJumping;
    public bool walk, attack, guard, idle;

    [Header("Drag and Drop")]
    public Rigidbody2D rb2d;
    public Animator animator;
    [Header("Static Vars")]
    public int zero = 0;





    public virtual void Start()
    {      
        rb2d = GetComponent<Rigidbody2D>();
        animator = GetComponent<Animator>();
    }

    public virtual void CheckHealth()
    {
        if (health <= zero)
        {
            StartCoroutine("Die");
        }
    }
    IEnumerator Die()
    {
        yield return new WaitForSeconds(0.5f);
        //animator.SetTrigger("Die");
    }

    public virtual void TakeDamage(int dmg)
    {
        if (health > zero)
        {
            health -= dmg;
        }
    }
  
    public void ChangeDirection()
    {

        if (x > 0 && !facingRight || x < 0 && facingRight)
        {
            facingRight = !facingRight;
            transform.Rotate(0, 180, 0);

        }
    }
    //////////////////--------------- GETTERS AND SETTERS -------------------//////////////////////////////////////////

    public void SetHealth(int maxHealth)
    {
        health = maxHealth;
    }
    public void SetPosition(float _x, float _y)
    {
        x = _x;
        y = _y;
    }
    public float GetPositionX()
    {
        return x;
    }
    public float GetPositionY()
    {
        return y;
    }
    public void SetAttackFalse()
    {
        attack = false;
    }

    public bool GetFightingStatus()
    {
        return isFighting;
    }
    public void SetFightingStat(bool fighting_)
    {
        isFighting = fighting_;
    }

    public int GetBasicAttack()
    {
        return basicAtack;
    }

    public void SetBasicAttack(int dmg)
    {
        basicAtack = dmg;
    }
  

}
