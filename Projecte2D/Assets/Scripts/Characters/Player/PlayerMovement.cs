﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerMovement : BasicStats
{
    public enum PlayerState { Idle, IdleFly, Attack, Walk, Fly, Guard, Die, Dash}
    public PlayerState currentPlayerState;
    [Header("Child Script")]
    public bool isPlayingAnimation;
    [Header("Vars to move")]
    public float flySpeed;
    public bool flying;
    public bool landing;
    protected float speedo;
    private bool dashing;
    [Header("Vars to fight")]

    [Header("Drag and Drop")]
    public Controller2D controller;
    public FightMap fm;
    public Transform revivePoint;
    public HealthBar healthBar;
    [HideInInspector] public Enemy ene;
    [HideInInspector] public GameObject e;

    public void VarAssignment()
    {
        speedo = 100f;
        runSpeed = speedo;
        flySpeed = runSpeed;
        landing = false;
        isFighting = false;
        guard = false;
        attack = false;
        flying = true;
        canWeMove = true;
        maxHealth = 100;
        SetHealth(maxHealth);
        healthBar.SetHealth(maxHealth);  
        Time.timeScale = 1f;
        currentPlayerState = PlayerState.Idle;
    }
   
    [Header("RayCast")]

    // TODO: Mirare de millorar aquesta part i fer servir nomes un raycast
    RaycastHit2D hit;
    int layerMask;
    public float rayCastLength;
    public int layerGround = 8; 
    public bool isGrounded;
    private void Awake()
    {
        VarAssignment();
    }

    private void Update()
    {
        healthBar.SetHealth(health);
    }
    public void FixedUpdate()
    {
        RayCastManagment();
        if (canWeMove == true && isFighting == true)
        {
            AnimationManagment();
            MoveForFight();

        }
        else if (canWeMove == false)//count 3 2 1 fight
        {
            if (currentPlayerState != PlayerState.Idle)
            {
                SetCurrentState(PlayerState.Idle);
            }
        }
        else if(canWeMove == true && isFighting == false)
        {
            AnimationManagment();
            MoveInWorld();
        }
    }

    ///Inputs and movement managment
    
    public void MoveForFight()
    {       
        x = Input.GetAxisRaw("Horizontal") * runSpeed;       
        controller.Move(x * Time.deltaTime, isJumping);
        isJumping = false;
        ChangeDirection();
    }
    public void MoveInWorld()
    {

        x = Input.GetAxisRaw("Horizontal") * runSpeed;

        if(flying == true)
        {
            y = Input.GetAxisRaw("Vertical") * flySpeed;
        }

        if ( landing == true)//aqui ja ha aterrizat
        {
          
            controller.Move(x * Time.deltaTime, isJumping);
            isJumping = false;
        }
        else
        {
            controller.Fly(x * Time.deltaTime, y * Time.deltaTime);
        }
        ChangeDirection();

    }
    public void AnimationManagment()
    {
        if (IsPlayingAnyAnimation() == false)
        {
            if(x != 0)
            {
                switch (flying)
                {
                    case true:
                        if (currentPlayerState != PlayerState.Fly)
                        {
                            SetCurrentState(PlayerState.Fly);
                        }
                        break;
                    case false:
                        
                            SetCurrentState(PlayerState.Walk);
                        
                        break;
                    default:
                        SetCurrentState(PlayerState.Fly);
                        break;
                }

                
            }
             if (x == 0)
            {
                switch (flying)
                {
                    case true:
                        if (currentPlayerState != PlayerState.IdleFly)
                        {
                            SetCurrentState(PlayerState.IdleFly);
                        }
                        break;
                    case false:
                     
                            SetCurrentState(PlayerState.Idle);
                        
                        break;
                    default:
                            SetCurrentState(PlayerState.Idle);
                        
                        break;
                }

            }
        }

        
    }
    public void MoveToRevivePoint()
    {
        //transform.position = revivePoint.transform.position;
        //health = maxHealth;
        //healthBar.SetHealth(maxHealth);
        //flying = true;
        //animator.SetTrigger("IdleFlying");
        //isJumping = false;
        //SetFightingStat(false);
    }
    public void RayCastManagment()
    {
        layerMask = 1 << layerGround;
        hit = Physics2D.Raycast(transform.position, -Vector2.up, rayCastLength, layerMask);
        Debug.DrawRay(transform.position, -Vector2.up, Color.red);

        if (hit)
        {
            isGrounded = true;
        }
        else
        {
            isGrounded = false;
        }
    }
    //////////////////////////----------COLLISIONS-------////////////////////////////////
    public void OnTriggerEnter2D(Collider2D other)
    {

        if (other.CompareTag("Enemy") && this.isFighting == false)
        {
            this.isFighting = true;
            e = other.gameObject;


            fm.SetPlayerAndEnemyToFightMap(gameObject, e);
            SetCurrentState(PlayerState.Idle);
            x = 0;


        }
        ene = other.gameObject.GetComponent<EnemyFight>();
        // Debug.Log("tag - " + this.tag);

        if (other.CompareTag("EnemyWeapon") && !animator.GetCurrentAnimatorStateInfo(0).IsName("HoldWardPlayer"))
        {
           
            TakeDamage(e.GetComponent<EnemyFight>().basicAtack);
          
            

        }
    }
    //////////////////////////----------GETTERS AND SETTERS-------////////////////////////////////

    public override void TakeDamage(int dmg)
    {
        base.TakeDamage(dmg);
        healthBar.SetHealth(health);
        CheckHealth();
    }

    //////////////////////////----------Animations------////////////////////////////////
    public void ChangeAnimation(string anim)
    {
        animator.SetTrigger(anim);
    }
    public void SetCurrentState(PlayerState ps)
    {
       
        switch (ps)
        {
            case PlayerState.Walk:
                ChangeAnimation("Walking");
                break;
            case PlayerState.Attack:
                ChangeAnimation("Attack");
                break;
            case PlayerState.Guard:
                ChangeAnimation("Guard");
                break;
            case PlayerState.Idle:
                ChangeAnimation("Idle");
                break;
            case PlayerState.Fly:
                ChangeAnimation("Fly");
                break;
            case PlayerState.IdleFly:
                ChangeAnimation("IdleFlying");
                break;
            case PlayerState.Die:
                ChangeAnimation("Die");
                break;
            case PlayerState.Dash:
                ChangeAnimation("Dash");
                break;
            default:
                break;
        }
        currentPlayerState = ps;
    }
    public PlayerState GetCurrentState()
    {
        return currentPlayerState;

    }
    public bool IsPlayingAnyAnimation()
    {
        isPlayingAnimation = false;
        if (animator.GetCurrentAnimatorStateInfo(0).IsName("attack") || animator.GetCurrentAnimatorStateInfo(0).IsName("Dash")
            || animator.GetCurrentAnimatorStateInfo(0).IsName("guardia") || animator.GetCurrentAnimatorStateInfo(0).IsName("HoldWardPlayer"))
        {
            isPlayingAnimation = true;
        }
        return isPlayingAnimation;
    }

    public bool GetFlyingStatus()
    {
        return flying;
    }
    public void SetFlyingStat(bool flying_)
    {
        flying = flying_;
    }
    public void KeepWalking()
    {
        if (x != 0)
        {
            SetCurrentState(PlayerState.Walk);
        }
    }
}

