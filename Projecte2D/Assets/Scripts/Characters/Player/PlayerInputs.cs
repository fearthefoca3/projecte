﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerInputs : PlayerMovement
{

    public float fallMultiplier = 2.5f;
    public float lowJumpMultiplier = 2f;
    public int dashPower;
    public GameObject PCollider;
    private float dashCD = 1f;
    public bool canDash = true;
    private void Update()
    {
        InputAttackDefend();
        InputManagment();
        Jump();
    }
    void InputManagment()
    {
        if (isFighting && canWeMove)
        {
            if (Input.GetButtonDown("Jump"))
            {
                isJumping = true;


            }

            if (Input.GetButtonDown("Jump") && !flying)
            {
                isJumping = true;


            }
            if (Input.GetKeyDown("e") && canDash)
            {

                
                canDash = false;
                if (facingRight)
                {
                    rb2d.velocity = Vector2.right * dashPower;
                }
                else
                {
                    rb2d.velocity = Vector2.left * dashPower;

                }
                StartCoroutine("DasCoolDown");





            }
        }
        

        if (Input.GetKeyDown("s") && isGrounded)
        {
            landing = true;
            flying = false;
            animator.SetTrigger("Idle");
        }


        if (!isFighting && Input.GetKeyDown("w"))
        {
            landing = false;
            flying = true;
            animator.SetTrigger("IdleFlying");

        }
        

    }
    public void InputAttackDefend()
    {
        if (!flying)
        {
            if (Input.GetMouseButtonDown(0))
            {
                attack = true;
                ChangeAnimation("Attack");
            }
            if (Input.GetMouseButtonDown(1))
            {
                guard = true;
                runSpeed = 0;
                flySpeed = 0;
                animator.SetBool("Guard", true);
            }
            if (Input.GetMouseButtonUp(1))
            {
                animator.SetBool("Guard", false);
                runSpeed = speedo;
                flySpeed = speedo;
                guard = false;
            }
        }

    }
    public void Jump()
    {
      
            //rb2d.AddForce(new Vector2(0f, jumpForce));
            if (rb2d.velocity.y < 0 && !flying)
            {
                rb2d.velocity += Vector2.up * Physics2D.gravity.y * (fallMultiplier - 1) * Time.deltaTime;
            }
            else if(rb2d.velocity.y > 0 && !Input.GetButton("Jump") && !flying)
            {
                rb2d.velocity += Vector2.up * Physics2D.gravity.y * (lowJumpMultiplier - 1) * Time.deltaTime;
            }
        
    }
    IEnumerator DasCoolDown()
    {
        SetCurrentState(PlayerState.Dash);
        yield return new WaitForSeconds(dashCD);
        SetCurrentState(PlayerState.Idle);
        canDash = true;
    }

}
