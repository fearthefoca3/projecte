﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Controller2D : MonoBehaviour
{

    public Rigidbody2D rb2d;
    private Vector3 rb2dVelocity = Vector3.zero;

    [Range(0, .45f)]
    [SerializeField]
    private float MovementSmoothing = .05f;

    public float rayCastLength = 10f;
    public int layerGround = 8; // Layer contra la que colisiona el player per aterrar
    public bool isGrounded; //Variable privada, ESTA EN PUBLIC PER FER PROVES
    public float jumpForce = 3000f;
    [Range(1,100)]public float jumpVelocity;
    public float fallMultiplier= 2.5f;
    public float lowJumpMultiplier = 2f;
    RaycastHit2D hit;
    int layerMask;

    int gravityForceScale = 3;

    // Start is called before the first frame update
    void Awake()
    {
        rb2d = GetComponent<Rigidbody2D>();
    }

    // Update is called once per frame
    void FixedUpdate()
    {
        layerMask = 1 << layerGround;

        hit = Physics2D.Raycast(transform.position, -Vector2.up, rayCastLength, layerMask);
        Debug.DrawRay(transform.position, -Vector2.up * rayCastLength, Color.red);
  
        if (hit)
        {
            isGrounded = true;
        }
        else
        {
            isGrounded = false;
        }
    }

    public void Move(float move, bool jumping, bool landing = false)
    {
        rb2d.gravityScale = gravityForceScale;

        Vector3 targetVelocity = new Vector2(move * 10f, rb2d.velocity.y);

        rb2d.velocity = Vector3.SmoothDamp(rb2d.velocity, targetVelocity, ref rb2dVelocity, MovementSmoothing);

        if (isGrounded & jumping)
        {
            //rb2d.AddForce(new Vector2(0f, jumpForce));
            rb2d.velocity = Vector2.up * jumpVelocity;
        }
    }

    public void Fly(float x, float y)
    {
        rb2d.gravityScale = 0;
        Vector3 targetVelocity = new Vector2(x * 10f, y * 10f);
        rb2d.velocity = Vector3.SmoothDamp(rb2d.velocity, targetVelocity, ref rb2dVelocity, MovementSmoothing);

    }
}
