﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Enemy : BasicStats
{
    public int MeleeRange;

    // Start is called before the first frame update
    void Awake()
    {
        health = 100;
    }

    // Update is called once per frame
    void Update()
    {

        ChangeDirection();
        SwapAnim();
        
    }

    
    void SwapAnim()
    {
        if(walk == true)
        {
            animator.SetTrigger("Walking");
            attack = false;
            guard = false;
            idle = false;
        }
        if(attack == true)
        {
            animator.SetTrigger("Attack");
            
            walk = false;
            guard = false;
            idle = false;
        }
        if (guard == true)
        {
            animator.SetTrigger("Guard");
            attack = false;
            walk = false;
            idle = false;
        }
        if(idle == true)
        {
            animator.SetTrigger("Idle");
            attack = false;
            walk = false;
            guard = false;
        }
    }
   
    
}
