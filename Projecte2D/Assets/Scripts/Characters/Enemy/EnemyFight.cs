﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyFight : Enemy
{
    public enum EnemyState { Idle, Attack, Move, Guard, Walk, Dash}
    public EnemyState currentState;

    [Header("Vars to move")]
    public bool gir;
    public bool isPlayingAnimation;
    public bool isactive;
    public int atackPercerntage;
    public int guardPercentage;
    public int dodgePercentage;
    private int randomMax;
    int randomAction;
    public bool animationRuning;
    public int DashSpd;
    private float timerForCD;
    public float enemyDificulty;

    [Header("Drag and Drop")]

    public GameObject player;
    public PlayerInputs pla;
    private Vector3 rb2dVelocity = Vector3.zero;
    public FightMap fm;
    private Vector2 dist;
    private Vector3 targetVelocity;

    // UI
    public EnemyHealthbar healthBar;
    public GameObject ShieldAnim;


    public bool Dashing;
    public bool canDash;
    public bool test;

    // Start is called before the first frame update
    public override void Start()
    {
        base.Start();
        randomMax = atackPercerntage + guardPercentage + dodgePercentage;
        enemyDificulty = 0f;
        DashSpd = -10;
        player = GameObject.FindGameObjectWithTag("Player");
        pla = GetComponent<PlayerInputs>();
        fm = FindObjectOfType<FightMap>();
        maxHealth = 100;
        health = maxHealth;
        SetHealth(maxHealth);
        canWeMove = false;
        isFighting = true;
        MeleeRange = 5;
        //RandomizeEnemyActions();
        currentState = EnemyState.Idle;
        canDash = true;
        timerForCD = 0;
    }

    // Update is called once per frame
    void Update()
    {
        healthBar.SetHealth(health);

        //ChangeDirection();
        if (canWeMove == true && isFighting)
        {
            rb2d.gravityScale = 1;
            Movement();
            CheckHealth();
        }
        else
        {
            if (currentState != EnemyState.Idle)
            {
                SetCurrentState(EnemyState.Idle);
            }
        }
        if(currentState == EnemyState.Guard)
        {
            ShieldAnim.SetActive(true);
        }
        else
        {
            ShieldAnim.SetActive(false);
        }
        timerForCD += 1 * Time.deltaTime;
    }
   

    void Movement()
    {
        if (player != null)
        {
            dist = player.transform.position - transform.position;


            //change direction ens mira si hem de girar o no     
            ChangeEnemyDirections();

            targetVelocity = new Vector2(-runSpeed, rb2d.velocity.y);

            if (!facingRight && dist.x > -MeleeRange || facingRight && dist.x < MeleeRange)
            {
                targetVelocity = new Vector2(0, 0);
                if(timerForCD >= enemyDificulty)
                {
                    RandomizeEnemyActions();
                    timerForCD = 0;

                }
                
            }
            else if (currentState != EnemyState.Walk )
            {               
                SetCurrentState(EnemyState.Walk);
            }

            if (Dashing)
            {
                targetVelocity.x *= DashSpd;

            }
            rb2d.velocity = Vector3.SmoothDamp(rb2d.velocity, targetVelocity, ref rb2dVelocity, 0);

        }



    }
    public void RandomizeEnemyActions()
    {
        if (!IsPlayingAnyAnimation())
        {

            randomAction = Random.Range(0, randomMax);
            int switchCase = 0;
            if (randomAction <= atackPercerntage)
            {
                switchCase = 1;
            }
            else if (randomAction > atackPercerntage && randomAction < guardPercentage + atackPercerntage)
            {
                switchCase = 2;

            }
            else if (randomAction > guardPercentage + atackPercerntage)
            {
                switchCase = 3;
            }
            switch (switchCase)
            {
                case 1:
                    if (currentState != EnemyState.Attack)
                    {
                        SetCurrentState(EnemyState.Attack);
                    }

                    break;
                case 2:
                   
                    if (currentState != EnemyState.Guard)
                    {
             
                        SetCurrentState(EnemyState.Guard);      
                    }

                    break;
                case 3:
                    if (currentState != EnemyState.Guard)
                    {
                        SetCurrentState(EnemyState.Guard);
                    }

                    break;
                default:
                    break;
            }
        }

    }

    public void ChangeAnimation(string anim)
    {
        animator.SetTrigger(anim);
    }
    public void ChangeEnemyDirections()
    {
        if (facingRight && dist.x < 0 || !facingRight && dist.x > 0) // basicament si el player esta a la dreta o esquerra gira
        {
            facingRight = !facingRight;
            transform.Rotate(0, 180, 0);
            runSpeed = runSpeed * -1;


        }
    }

    public bool IsPlayingAnyAnimation()
    {
        isPlayingAnimation = false;
        if(animator.GetCurrentAnimatorStateInfo(0).IsName("attack")|| animator.GetCurrentAnimatorStateInfo(0).IsName("idle") || animator.GetCurrentAnimatorStateInfo(0).IsName("DashEnemy")
            || animator.GetCurrentAnimatorStateInfo(0).IsName("holdGuardAnimation") || animator.GetCurrentAnimatorStateInfo(0).IsName("guardia"))
        {
            isPlayingAnimation = true;
        }
        return isPlayingAnimation;
    }

    public bool AnimationsNotMoveX()
    {
        isactive = false;
        if (animator.GetCurrentAnimatorStateInfo(0).IsName("attack") ||  animator.GetCurrentAnimatorStateInfo(0).IsName("DashEnemy")
            || animator.GetCurrentAnimatorStateInfo(0).IsName("holdGuardAnimation") || animator.GetCurrentAnimatorStateInfo(0).IsName("guardia"))
        {
            isactive = true;
        }
        return isactive;
    }
    public override void CheckHealth()
    {

        if (health <= zero)
        {
            GameObject p = GameObject.FindGameObjectWithTag("Player");
            fm.ReturnPlayerToWorld(p);
            StartCoroutine("DieEnemy");
        }
    }
    IEnumerator DieEnemy()
    {
        yield return new WaitForSeconds(0.5f);
        gameObject.SetActive(false);
    }

    public override void TakeDamage(int dmg)
    {

        base.TakeDamage(dmg);

        healthBar.SetHealth(health);
        CheckHealth();
    }


    //////////////////////////----------COLLISIONS-------////////////////////////////////
    private void OnCollisionEnter2D(Collision2D other)
    {



    }
    private void OnTriggerEnter2D(Collider2D other)
    {
        

        if (other.CompareTag("AtackingWeapon"))
        {
            if (currentState != EnemyState.Guard)
            {
                TakeDamage(player.GetComponent<PlayerInputs>().basicAtack);
            }
            else
            {
            }
                
        }
    }


    //////////////////--------------- GETTERS AND SETTERS -------------------//////////////////////////////////////////

    public void SetCurrentState(EnemyState es)
    {
        //Debug.LogWarning("currentState: "+ currentState + " TargetState: "+ es);
        switch (es)
        {
            case EnemyState.Walk:
                ChangeAnimation("Walk");
                break;
            case EnemyState.Attack:
                ChangeAnimation("Attack");
                break;
            case EnemyState.Guard:
                ChangeAnimation("Guard");
                //StartCoroutine(Guarding());
                break;
            case EnemyState.Idle:
                ChangeAnimation("Idle");
                break;
            case EnemyState.Dash:
                ChangeAnimation("Dash");
                DashEnemy();
                break;
           
            default:
                break;
        }
        currentState = es;
    }
    public EnemyState GetCurrentState()
    {
        return currentState;

    }
    public void SetAnimationRuning()
    {
        animationRuning = false;
       
    }
    public void DashEnemy()
    {

            Dashing = true;
            //StartCoroutine(CDDashing());

        

    }

    public void stopDash()
    {
        Dashing = false;

        animator.SetTrigger("Walk");

    }
    /*
    IEnumerator Guarding()
    {
        Debug.Log("start g");
        float OriginalRUnSpeed = runSpeed;
        runSpeed = 0;
        animator.SetTrigger("Guard");
        yield return new WaitForSeconds(3);
       // animator.SetTrigger("Walk");
        runSpeed = OriginalRUnSpeed;
        Debug.Log("end g");
    }*/

    public IEnumerator CDDashing()
    {
        
        yield return new WaitForSeconds(3 );
        canDash = true;
        
    }

}
