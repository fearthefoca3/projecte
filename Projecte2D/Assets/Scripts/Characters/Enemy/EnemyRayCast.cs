﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyRayCast : EnemyFight
{

    public Transform EdgeDetectionPoint;
    public LayerMask WhatIsGround;
    public LayerMask WhatIsPlayer;
    public float DetectionRange;
    [Range(0,360)]public float VisionAngle;
    
    public List<Transform> playerss;

    

    float attackCd = 1f;
    float currentAttackCd = 0f;

    // Start is called before the first frame update
    void Start()
    {
        animator = GetComponent<Animator>();
        rb2d = GetComponent<Rigidbody2D>();
     
    }

    // Update is called once per frame
    void Update()
    {
        currentAttackCd -= Time.deltaTime;

        if (attack && currentAttackCd <= 0)
        {
            currentAttackCd = attackCd;
        }


        if (EdgeDetected())
        {
            Debug.Log("Peta aqui clicam");
            //if (playerss[0].transform.position.x - transform.position.x > 0 && !facingRight || playerss[0].transform.position.x - transform.position.x < 0 && facingRight)
            //{

            //    facingRight = !facingRight;
            //    transform.Rotate(0, 180, 0);

            //}
        }
            

        if(PlayerInRange(ref playerss))
        {
            if (PlayerInAngle(ref playerss))
            {
                animator.SetTrigger("Walking");
                Chase(playerss[0].transform);
            }
        }
       
        
        
        
    }
    private bool EdgeDetected()
    {
        RaycastHit2D hit = Physics2D.Raycast(EdgeDetectionPoint.position, -Vector2.up, 1.5f, WhatIsGround);
        return hit.collider == null;
    }
  
    public void Chase(Transform playerTransform)
    {
        float distance = playerTransform.position.x - transform.position.x;
        if ((distance <= 1.5 && distance > 0) || (distance >= -1.5 && distance < 0))
        {
            animator.SetTrigger("Attack");
            attack = true;
        }
        else
        {
            transform.Translate(transform.right * Time.deltaTime * runSpeed, Space.World);
            attack = false;
        }
    }

    private void OnDrawGizmos()
    {
        Gizmos.DrawWireSphere(transform.position, DetectionRange);

        Gizmos.color = Color.red;
        var direction = Quaternion.AngleAxis(VisionAngle / 2, transform.forward) * transform.right;
        Gizmos.DrawRay(transform.position, direction * DetectionRange);
        var direction2 = Quaternion.AngleAxis(-VisionAngle / 2, transform.forward) * transform.right;
        Gizmos.DrawRay(transform.position, direction2 * DetectionRange);

        Gizmos.color = Color.white;
    }

    private bool PlayerInRange(ref List<Transform> players)
    {
        Collider2D[] playerColliders = Physics2D.OverlapCircleAll(transform.position, DetectionRange, WhatIsPlayer);
       
        foreach (var item in playerColliders)
        {
            players.Add(item.transform);
        }

        return true;
    }

    private bool PlayerInAngle(ref List<Transform> players)
    {
        for (int i = players.Count - 1; i >= 0; i--)
        {
            var angle = GetAngle(players[i]);

            if (angle > VisionAngle / 2)
            {
                players.Remove(players[i]);
            }
        }
        animator.SetTrigger("Attack");
        attack = false;

        return players.Count > 0;
    }

    private float GetAngle(Transform target)
    {
        Vector3 targetDir = target.position - transform.position;
        float angle = Vector3.Angle(targetDir, transform.right);
        return angle;
    }

}
