﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.IO;
using System.Linq;
using System;
public class GameInformationSaved : MonoBehaviour
{
    [SerializeField] PlayerMovement player;
    [Header("File Saved Info")]
    List<string> lines = new List<string>();
    public string filePath;
    private void Awake()
    {
        player = FindObjectOfType<PlayerMovement>();
        CreateText();
        filePath = Application.dataPath + "/Scripts/SavingInfo/Log.txt";
        lines = File.ReadAllLines(filePath).ToList();
        //ReadText();
        SetPlayerStats();

    }
    void Start()
    {
        
    }

    void CreateText()
    {
        //direcció del archiu
        filePath = Application.dataPath + "/Scripts/SavingInfo/Log.txt";
        //mirar si el arxiu existeix i si no el crea
        if(!File.Exists(filePath))
        {
            File.WriteAllText(filePath, "This is the Saved Data \n\n");
            File.AppendAllText(filePath, "PlayerPosX:0:PlayerposY:0\n");
        }
    }
    void  ReadText()
    {   //aixo llegeix el document txt.
        foreach(string line in lines)//per cada linea del text es guardara en una array cada element separat per una coma ,
        {
            string[] items = line.Split(',');//<-- es guarda a items els elements de la coma
            //Enemy p = new Enemy(items[0],items[1]...)
            //i el afegim a una llista per despres crearlos
        } 
    }
    void SetPlayerStats()
    {
        string playerPosit = File.ReadLines(filePath).Skip(2).Take(1).First();
        string[] infoSaved = playerPosit.Split(':');
        //player.SetPosition(float.Parse(infoSaved[1]), float.Parse(infoSaved[3]));
      //  Debug.Log(player.GetPositionX());
    }
    /*
    void MovePlayerToCheckPoint()
    {
        string playerPosit = File.ReadLines(filePath).Skip(3).Take(1).First();
        string[] infoSaved = playerPosit.Split(':');
        player.SetPosition(float.Parse(infoSaved[1]), float.Parse(infoSaved[3]));
        //  Debug.Log(player.GetPositionX());
    }
    */
    void WriteText()
    {
        File.WriteAllText(filePath, "This is the Saved Data \n\n");
        File.AppendAllText(filePath,"PlayerPosX:"+ player.GetPositionX() +":PlayerposY:"+ player.GetPositionY() + "\n");
        File.AppendAllText(filePath, "CheckPointPosX:" + player.GetPositionX() + ":CheckPointposY:" + player.GetPositionY() + "\n");
    }
    void OnApplicationQuit()
    {
        WriteText();
    }
 
    
}