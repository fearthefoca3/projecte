﻿
using UnityEngine;

public class CameraController : MonoBehaviour
{
    public Transform target;
    public PlayerMovement p;
    public FightMap fm;


    public float smoothSpeed = 0.125f;
    public bool fighting;
    public Vector3 offset = new Vector3(0, 0, -10);
    public Vector3 finalPos;

    private int cameraFightSize = 40;
    private int cameraWorldSize = 30;


    private void Start()
    {
        fighting = false;
        fm = FindObjectOfType<FightMap>();
        p = FindObjectOfType<PlayerMovement>();
    }

    private void FixedUpdate() //si no va be fer servir LateUpdate
    {
        if (p.GetFightingStatus())
        {
            transform.position = fm.MoveCameraToFight();
            Camera.main.orthographicSize = cameraFightSize;
        }
        else
        {
            
            Camera.main.orthographicSize = cameraWorldSize;
            if (fighting)
            {
                transform.position = target.position + offset;
                fighting = false;
            }
            else
            {
                finalPos = target.position + offset;
                transform.position = Vector3.Lerp(transform.position, finalPos, smoothSpeed);
            }  
        }

    }
}
