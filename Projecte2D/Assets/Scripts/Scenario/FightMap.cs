﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FightMap : MonoBehaviour
{
    public Transform cameraPositionFight;
    public GameObject mainCamera;
    public GameObject playerPosition;
    public GameObject playerBossPosition;
    public GameObject enemyPosition;
    private Vector2 worldPlayerPosition;
    private Vector2 worldEnemyPosition;
    private Vector3 oldCameraPos;

    private PlayerInputs player;
    private EnemyFight enemy;
    private CameraController mCamera;

    public GameObject EnemyHealthBar;

    public int FallDamage;
    public GameObject CountDown;

    private void Start()
    {
        player = FindObjectOfType<PlayerInputs>(); //TODO: mirar de canviar aixo pel PlayerInputs
        mCamera = FindObjectOfType<CameraController>();

        //  enemy = GameObject.FindGameObjectWithTag("Enemy");
    }

    public Vector3 MoveCameraToFight()
    {
        oldCameraPos = mainCamera.transform.position;
        return cameraPositionFight.position + new Vector3(0, 0, -10);
    }

    public void SetPlayerAndEnemyToFightMap(GameObject p, GameObject e)
    {
        p.GetComponent<PlayerInputs>().SetFlyingStat(false);
        if (worldPlayerPosition == Vector2.zero)
        {
            worldPlayerPosition = p.transform.position;
        }

        if (e != null)
        {

            mCamera.fighting = true;
            worldEnemyPosition = e.transform.position;
            p.transform.position = playerPosition.transform.position;
            e.transform.position = enemyPosition.transform.position;
            StartCoroutine("Wait3seconds");
            e.GetComponent<EnemyFight>().SetFightingStat(true);

            enemy = e.GetComponent<EnemyFight>();
            EnemyHealthBar.gameObject.SetActive(true);

        } else
        {
            mCamera.fighting = false;
            p.transform.position = playerPosition.transform.position;
         
        }

    }
    public void ReturnPlayerToWorld(GameObject p)
    {
     
        p.transform.position = worldPlayerPosition;
        p.GetComponent<PlayerInputs>().SetFightingStat(false);
        EnemyHealthBar.SetActive(false);
        mCamera.fighting = true;
        
        ItemWorld.SpawnItemWorld(transform.position, new Item { itemType = Item.ItemType.HealthPotion, amount = 1 });
        // worldPlayerPosition = Vector2.zero;
        /*
        if (e.GetComponent<CircleCollider2D>().gameObject != null)
        {
            e.transform.position = worldEnemyPosition;
            e.GetComponent<CircleCollider2D>().gameObject.SetActive(false);
        }
        */
        CountDown.SetActive(false);

    }
    public void ReturnEnemyToWorld(GameObject e)
    {
        if (e != null)
        {
            e.transform.position = worldEnemyPosition;
            enemy = e.GetComponent<EnemyFight>();
            enemy.SetFightingStat(false);
            enemy.health = enemy.maxHealth;
            //--Falta posar lo de la barra de vida que no esta activa encara falta ajuntar versio goldero
            Debug.Log("llegir comentari aqui");
            enemy.animator.SetTrigger("Idle");
            enemy.rb2d.gravityScale = 0;

            EnemyHealthBar.gameObject.SetActive(false);
        }

    }
    public void prepareBossfight(GameObject p)
    {
        mCamera.fighting = true;
        worldPlayerPosition = p.transform.position;
        p.transform.position = playerBossPosition.transform.position;

        player.flying = false;
    }

    void Update()
    {
        if (enemy != null && enemy.GetComponent<Enemy>().health <= 0 && player.GetFightingStatus() == true)
        {
           ReturnPlayerToWorld(player.gameObject);
            CountDown.SetActive(false);

        }
    }
    IEnumerator Wait3seconds()
    {

        CountDown.SetActive(true);
        if (enemy != null)
        {
           
            enemy.canWeMove = false;
        }
        player.canWeMove = false;
        player.rb2d.velocity = new Vector2(0, 0);
        yield return new WaitForSeconds(3f);
        enemy.canWeMove = true;
        player.canWeMove = true;
        CountDown.SetActive(false);
    }

    private void OnTriggerEnter2D(Collider2D other) // Per detectar quan algú es cau del mapa de combat
    {
        if (other.CompareTag("Player"))
        {
            other.transform.position = playerPosition.transform.position;
            other.GetComponent<PlayerInputs>().TakeDamage(FallDamage);
        }
    }
}
