﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Obstacle : MonoBehaviour
{
    public int damage = 10;

    public void OnCollisionEnter2D(Collision2D collision)
    {

        if (collision.gameObject.GetComponent<PlayerInputs>() != null)
        {
            PlayerInputs player = collision.gameObject.GetComponent<PlayerInputs>();
            player.TakeDamage(damage);
            Debug.Log(player.health);
        }
    }
}