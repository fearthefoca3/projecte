﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CoreBoss : MonoBehaviour
{
    public int HitsCanResist;
    public int Hits;

    // Start is called before the first frame update
    void Start()
    {
        Hits = 0;
    }

    public void takeDamage()
    {
        Hits++;
        if (Hits >= HitsCanResist)
        {
            FindObjectOfType<BossFight>().CoreDestroyed();
            this.gameObject.SetActive(false);
        }
    }
}
