﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Explosion : MonoBehaviour
{
    public int DamageToPlayer;
    public GameObject Player;


    private void OnTriggerEnter2D(Collider2D other)
    {
        if (other.gameObject.tag == "CoreBoss")
        {
            other.GetComponent<CoreBoss>().takeDamage();
        }
        if (other.gameObject.tag == "Player")
        {
            other.GetComponent<PlayerMovement>().TakeDamage(DamageToPlayer);
        }
    }

    void EndExplosion()
    {
        Destroy(this.gameObject);   
    }

}
