﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class PlayerInventory : MonoBehaviour
{
    [SerializeField] private UI_Inventory uiInventory;

    private Inventory inventory;
    public PlayerInputs player;

    [Header("Item Vars")]
    public int HpRestore;
    public int DmgIncrease;
    public int SpdIncrease;
    public int SpdIncreaseDuration;

    [Header("Visual effects")]
    public Image HPBorder;
    public Sprite DefaultSprite;
    public Sprite SpdBuffSprite;
    
    private void Awake()
    {
        player = GetComponent<PlayerInputs>();
        inventory = new Inventory(UseItem);
        uiInventory.SetInventory(inventory);

        //Spawnejar items -- ItemWorld.SpawnItemWorld(transform.position, new Item { itemType = Item.ItemType.HealthPotion, amount = 1 });
        /**ItemWorld.SpawnItemWorld(transform.position + new Vector3(20, 20), new Item { itemType = Item.ItemType.HealthPotion, amount = 1 });
        ItemWorld.SpawnItemWorld(transform.position + new Vector3(30, 30), new Item { itemType = Item.ItemType.SpeedPotion, amount = 1 });
        ItemWorld.SpawnItemWorld(transform.position + new Vector3(40, 40), new Item { itemType = Item.ItemType.HealthPotion, amount = 1 });
        ItemWorld.SpawnItemWorld(transform.position + new Vector3(50, 50), new Item { itemType = Item.ItemType.SpeedPotion, amount = 1 });**/
        ItemWorld.SpawnItemWorld(transform.position + new Vector3(50, 50), new Item { itemType = Item.ItemType.SpeedPotion, amount = 1 });

    }

    private void OnTriggerEnter2D(Collider2D collider)
    {
        ItemWorld itemWorld = collider.GetComponent<ItemWorld>();
        if (itemWorld != null)
        {
            //Toca item
            inventory.AddItem(itemWorld.GetItem());
            itemWorld.DestroySelf();
        }
    }

    private void UseItem(Item item)
    {
        switch (item.itemType)
        {
            case Item.ItemType.HealthPotion:
                //Function to do
                HealPotion(HpRestore);
                inventory.RemoveItem(new Item { itemType = Item.ItemType.HealthPotion, amount = 1 });
                break;

            case Item.ItemType.SpeedPotion:
                //Function to do
                StartCoroutine(SpdPotion());
                inventory.RemoveItem(new Item { itemType = Item.ItemType.SpeedPotion, amount = 1 });
                break;

            case Item.ItemType.PowerPotion:
                //Function to do
                inventory.RemoveItem(new Item { itemType = Item.ItemType.PowerPotion, amount = 1 });
                break;

        }
    }

    void HealPotion(int hp)
    {
        if (player.health + hp <= player.maxHealth)
        {
            player.health += hp;
        } else
        {
            player.health = player.maxHealth;
        }
    }
    IEnumerator SpdPotion()
    {
        player.runSpeed += SpdIncrease;
        player.flySpeed += SpdIncrease;
        HPBorder.sprite = SpdBuffSprite;
        yield return new WaitForSeconds(SpdIncreaseDuration);

        player.runSpeed -= SpdIncrease;
        player.flySpeed -= SpdIncrease;
        HPBorder.sprite = DefaultSprite;
    }

}
