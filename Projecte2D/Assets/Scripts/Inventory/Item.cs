﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;

[Serializable]
public class Item
{
    public enum ItemType
    {
        HealthPotion,
        SpeedPotion,
        PowerPotion,
    }

    public ItemType itemType;
    public int amount;

    public Sprite GetSprite()
    {
        switch (itemType)
        {
            default:
            case ItemType.HealthPotion:             return ItemAssets.Instance.healthPotionSprite;
            case ItemType.SpeedPotion:              return ItemAssets.Instance.speedPotionSprite;
            case ItemType.PowerPotion:              return ItemAssets.Instance.powerPotionSprite;
        }
    }

}

