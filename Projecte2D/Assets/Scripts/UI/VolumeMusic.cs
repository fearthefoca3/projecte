﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class VolumeMusic : MonoBehaviour
{
    // Start is called before the first frame update
    private AudioSource audioSC;
    private float musicVolume = 1f;
    void Start()
    {
        audioSC = GetComponent<AudioSource>();
    }

    // Update is called once per frame
    void Update()
    {
        audioSC.volume = musicVolume;
    }
    public void SetVolume(float vol)
    {
        musicVolume = vol;
    }
}
