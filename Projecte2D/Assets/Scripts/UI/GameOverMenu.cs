﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class GameOverMenu : MonoBehaviour
{
    public PlayerMovement player;
    public GameObject gameOverGameObject;
    private void Start()
    {
        player = FindObjectOfType<PlayerMovement>();
    }
    private void Update()
    {
        CheckGameOver();
    }
    public void ResetGame()
    {
        Time.timeScale = 1f;
        SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex);
        gameOverGameObject.SetActive(false);
        

    }

    void CheckGameOver()
    {
        if (player.health <= 0)
        {
            StartCoroutine("GameOverRoutine");
            Time.timeScale = 0f;
           // gameOverGameObject.SetActive(true);
        }
    }

    IEnumerator GameOverRoutine()
    {
        gameOverGameObject.SetActive(true);
        yield return new WaitForSeconds(10.0f);
        
        ResetGame();
    }
}
