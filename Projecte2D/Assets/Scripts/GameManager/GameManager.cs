﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class GameManager : MonoBehaviour
{
    public PlayerMovement player;
    public GameObject gameOverGameObject;
    private void Start()
    {
        player = FindObjectOfType<PlayerMovement>();
    }
    private void Update()
    {
        CheckGameOver();
    }
    public void ResetGame()
    {
        SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex);
        gameOverGameObject.SetActive(false);

    }

    void CheckGameOver()
    {
        if (player.health <= 0)
        {
            StartCoroutine("GameOverRoutine");
            ResetGame();
            gameOverGameObject.SetActive(false);
        }
    }

    IEnumerator GameOverRoutine()
    {
        gameOverGameObject.SetActive(true);
        yield return new WaitForSeconds(10.0f);
        ResetGame();
    }
 
}
